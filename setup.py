from setuptools import setup, find_packages


with open("README.md", "r") as fh:
    long_description = fh.read()
    
setup(name='simpleai',
      version='0.2.0',
      description='Simple IA search algorithms Library',
      author='Gael Marcadet',
      author_email='gael.marcadet@etu.univ-orleans.fr',
      long_description=long_description,
      long_description_content_type="text/markdown",
      dependancies=['graphviz', 'argparse'],
      packages=find_packages()
     )