##################################################
# File: adversary.py
# Description: Adversarial algorithms, including 
# MinMax and AlphaBeta.
# Version: 0.2.0
# Author: Gael Marcadet
##################################################

from abc import ABC, abstractmethod
from simpleai.core.generic import State

class AdversarialProblem( ABC ):

    @abstractmethod
    def successors( self, state : State, max_player : bool ):
        ''' Returns the successors of a state. '''
        pass

    @abstractmethod
    def is_solution( self, state : State, max_player : bool ) -> bool :
        ''' Returns True if state is a solution, False otherwise. '''
        pass

    def is_valid( self, state : State, max_player : bool ) -> bool:
        ''' Returns True if state is valid, False otherwise. '''
        return True

    @abstractmethod
    def initial_state( self ):
        ''' Returns the initial state of the problem. '''
        pass

class AdversarialHeuristic( ABC ):

    @abstractmethod
    def score( self, state : State, max_player : bool ):
        pass

from math import inf


class AdversarialMove:
    ''' Advsersarial results wraps result complexity to enhance retrocompatibility. '''
    def __init__( self, move : State, move_score : int, explored_nodes : int ):
        self.__move = move
        self.__score = move_score
        self.__explored_nodes = explored_nodes
    
    def move_state( self ) -> State:
        ''' Returns the state object which contains is the node. '''
        return self.__move

    def move_score( self ) -> int:
        ''' Returns computed score for this movement. '''
        return self.__score

    def move( self ):
        ''' Returns state's data which is actually provided object by problem's successors function. '''
        return self.__move.data

    def explored_nodes( self ):
        ''' Returns number of explored nodes. '''
        return self.__explored_nodes

class AdversarialAlgorithm( ABC ):
    @abstractmethod
    def best_move( self, state : State, max_player : bool, depth_limit = inf ) -> AdversarialMove:
        ''' Computes the best move from an initial state. '''
        pass



        

class MinMaxAlgorithm( AdversarialAlgorithm ):
    '''
    Implementation of MinMax algorithm in python.
    '''
    def __init__( self, problem : AdversarialProblem, heuristic : AdversarialHeuristic, verbose : bool = False ):
        '''MinMax constructor awaits problem and an heuristic to score states.'''
        self.problem = problem
        self.heuristic = heuristic
        self.verbose = verbose
        self.explored_nodes = 0

    def __minmax( self, state : State, curr_depth : int, limit_deph : int, max_player : bool ):
        self.explored_nodes += 1

        state.depth = curr_depth
        
        # minmax alogrithm ends when the current state is a terminal node (in tree) or a solution node. 
        if curr_depth == limit_deph or self.problem.is_solution( state, max_player ):
            # obviously, we must return current state and his associate score
            return ( state, None, self.heuristic.score( state, max_player ) )
        else:
            # in case where current state is not a solution and not a terminal node in tree
            # computes all state's successors scores, and keep the best
            # it performs in two step, fistly computes and stores successors and these results
            # the second step is to select the best score
            scores = []
            for successor in [ successor for successor in self.problem.successors( state, max_player ) ]:
                succ_result = self.__minmax( successor, curr_depth + 1, limit_deph, not max_player )
                if succ_result is not None:
                    scores.append( succ_result )
                
            # second step: keeps the best scores
            if scores != []:
                scores.sort( key = lambda s : s[ 2 ], reverse = max_player )
                current, successor, best_score = scores[ 0 ]
                return ( state, current, best_score )
                 

    def best_move( self, state, max_player : bool = True, depth_limit : int = inf ) -> AdversarialMove:
        self.explored_nodes = 0
        ''' Computes the best next move of the current state. '''
        # handles state also if not state (useful to develop quick programs)
        if type( state ) != State:
            state = State( state )
            
        _, best_next, best_score = self.__minmax( state, curr_depth = 0, limit_deph = depth_limit, max_player=max_player )  
        return AdversarialMove( best_next, best_score, explored_nodes = self.explored_nodes )
        



class AlphaBetaAlgorithm( AdversarialAlgorithm ):
    ''' AlphaBetaAlgorithm is based on MinMax idea, but it explores fewer nodes. Thanks to elagate method.'''
    def __init__( self, problem : AdversarialProblem, heuristic : AdversarialHeuristic ):
        self.problem = problem
        self.heuristic = heuristic
        self.explored_nodes = 0

    def __alpha_beta( self, state : State,  alpha, beta, max_player : bool, curr_depth, depth_limit ):
        self.explored_nodes += 1
        state.depth = curr_depth
        # Stops exploration when state is terminal or if depth limit is reached
        if curr_depth == depth_limit or self.problem.is_solution( state, max_player ): return state, self.heuristic.score( state, max_player )
        
        if max_player:
            # max player is maximizing minimal losses of min player
            action = None
            v = -inf
            for successor in self.problem.successors( state, max_player ):
                _, next_value = self.__alpha_beta( successor, alpha, beta, not max_player, curr_depth + 1, depth_limit  )
                if next_value > v:
                    action = successor
                    v = next_value
                
                # returns current successor and his best value
                if v >= beta : return successor, v
                alpha = max( alpha, v )
            
            # returns best action and his value
            return action, v
        else:
            action = None
            v = inf
            for successor in self.problem.successors( state, max_player ):
                _, next_value = self.__alpha_beta( successor, alpha, beta, not max_player, curr_depth + 1, depth_limit  )
                if next_value < v:
                    action = successor
                    v = next_value
                if v <= alpha : return successor, v
                # decrease beta
                beta = min( beta, v )
            # returns best action and his value
            return action, v

  
    def best_move( self, state , depth_limit : int = inf, max_player : bool = True ) -> AdversarialMove:
        self.explored_nodes = 0
        # handles state if not State object (useful to wite quicks program)
        if type( state ) != State:
            state = State( state )
        
        best_next, best_score  = self.__alpha_beta( state, -inf, inf, max_player=True, curr_depth = 0, depth_limit=depth_limit )
        return AdversarialMove( best_next, best_score, explored_nodes=self.explored_nodes )

