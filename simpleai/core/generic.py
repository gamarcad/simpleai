#################################################
# File: generic.py
# Description: Contains all generic objects and 
# methods usefull in different features context
#################################################

class State:
    ''' State represents a configuration of a problem, useful to limit complexity. '''
    def __init__( self, *data, **args ):
        try:
            if len( data ) == 1:
                self.data = data[ 0 ]
            else:
                self.data = data
                
        except:
            self.data = data

        self.cost = args.get( 'cost', None )
        self.depth = args.get( 'depth', 0 )
        if 'cost' in args:
            del args[ 'cost' ]
        self.args = args

    def get( self, arg ):
        return self.args[ arg ]


    def __str__( self ):
        return str( self.data )

    def __hash__(self):
        return hash( self.data )
    
    def __eq__(self, value):
        if type( value ) == State:
            return self.data == value.data
        else:
            return self.data == value

    def __ge__(self, value):
        if type( value ) == State:
            return self.data >= value.data
        else:
            return self.data >= value
    
    def __gt__(self, value):
        if type( value ) == State:
            return self.data > value.data
        else:
            return self.data > value
    
    def __le__(self, value):
        if type( value ) == State:
            return self.data <= value.data
        else:
            return self.data <= value
    
    def __lt__(self, value):
        if type( value ) == State:
            return self.data < value.data
        else:
            return self.data < value

    def __ne__(self, value):
        if type( value ) == State:
            return self.data != value.data
        else:
            return self.data != value
    
    def __len__( self ):
        return len( self.data )
    
    def __iter__( self ):
        return iter( self.data )

    def __getitem__( self, item ):
        return self.data[ item ]

    def __sub__( self, value ):
        if type( value ) == State:
            return self.data - value.data
        else:
            return self.data - value
    
    def __add__( self, value ):
        if type( value ) == State:
            return value.data + self.data
        else:
            return value + self.data
    
    def __mul__( self, value ):
        if type( value ) == State:
            return value.data * self.data
        else:
            return value * self.data
    
    def __div__( self, value ):
        if type( value ) == State:
            return self.data / value.data
        else:
            return self.data / value