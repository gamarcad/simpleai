from simpleai.core.generic import State
from simpleai.core.search.algorithms import SearchProblem

class HumanProblem( SearchProblem ):
    def __init__(self):
        self.left = 0
        self.right = 1
    
    def initial_state( self ):
        return State( 3, 3, self.left )
    
    def is_solution( self, state : State ) -> bool :
        ( canibals, humans, barq ) = state.data
        return canibals == 0 and humans == 0 and barq == self.right


    def is_valid( self, state : State ) -> bool:
        ( canibals, humans, barq ) = state.data
        return 0 <= canibals and 0 <= humans and canibals <= 3 and humans <= 3 and (
                canibals == humans or humans == 3 or humans == 0 
            )
            

    def successors(self, state : State ):
        (canibals, humans, barq) = state.data
        LEFT = self.left
        RIGHT = self.right
        if barq == LEFT:
            return [
                State( canibals - 1, humans, RIGHT ),
                State( canibals - 2, humans, RIGHT ),
                State( canibals, humans - 1, RIGHT ),
                State( canibals, humans - 2, RIGHT ),
                State( canibals - 1, humans - 1, RIGHT ),
            ]
        else:
            return [
                State( canibals + 1, humans, LEFT ),
                State( canibals + 2, humans, LEFT ),
                State( canibals, humans +1, LEFT ),
                State( canibals, humans +2, LEFT ),
                State( canibals + 1, humans + 1, LEFT ),
            ]
