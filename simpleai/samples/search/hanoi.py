from simpleai.core.generic  import State
from simpleai.core.search.algorithms import SearchProblem

class HanoiProblem( SearchProblem ):

    def initial_state(self):
        return State( 3, 0, 0 )
    
    def successors( self, state : State ):
        ( a, b, c ) = state.data
        return [
            State( a - 1, b + 1, c ),
            State( a - 1, b, c + 1 ),

            State( a + 1, b - 1, c ),
            State( a, b - 1, c + 1 ),

            State( a + 1, b, c - 1 ),
            State( a, b + 1, c - 1 ),
        ]

    def is_valid(self, state : State):
        state_sum = sum( state.data )
        ( a, b, c ) = state.data
        return 0 <= state_sum and state_sum <= 3 and 0 <= a and 0 <= b and 0 <= c

    def is_solution(self, state : State):
        return state == ( 0, 0, 3 )
